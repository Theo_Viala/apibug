<?php
require 'vendor/autoload.php';
use BugApp\Controllers\BugController;
use BugApp\Models\Identifiant;

$controller = new BugController();

$id = new Identifiant();

$method = $_SERVER['REQUEST_METHOD'];

$length = strlen($id->getBasePath());

$uri = substr($_SERVER['REQUEST_URI'], $length + 1);

// var_dump($_GET);

// die();

switch (true) {
    case $uri == "":
        header("Location: bug");
        break;     
    case preg_match('#^bug/(\d+)$#', $uri, $matches)  && $method == 'GET':
        $id = $matches[1]; 
        return $controller->show($id);
        break;
    case preg_match('#^bug(|\?.)#', $uri) && $method == 'GET':
        return $controller->list();
        break;   
    case preg_match('#^bug$#', $uri) && $method == 'POST':
        return $controller->add();
        break;
    case preg_match('#^bug/(\d+)$#', $uri, $matches)  && $method == 'PATCH':
        $id = $matches[1]; 
        $controller->edit($id);
        break;
    default:
        require '404.php';
}