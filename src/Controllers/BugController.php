<?php
namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use GuzzleHttp\Client;

class BugController{

    public function send($content,$code = 200){
        http_response_code($code);
        header("Content-type: application/json");
        echo $content;
    }

    /**
     * Ajoute un bug
     */
    public function add(){
        if(!empty($_POST['titre']) && !empty($_POST['description']) && !empty($_POST['url'])){
            $urlOk = $this->ControleUrl($_POST['url']);

            if ($urlOk["status"] == "success") {
                $today = new \DateTime();
                $today = $today->format('Y-m-d H:i:s');
                $bugManager = new BugManager("");
                $bugManager->addBdd(htmlspecialchars($_POST['titre']), htmlspecialchars($_POST['description']),$today, htmlspecialchars($_POST['url']), $urlOk["ip"]);    
                
                $content = [
                    'titre' => htmlspecialchars($_POST['titre']),
                    'description' => htmlspecialchars($_POST['description']),
                    'date' => $today,
                    'url' => htmlspecialchars($_POST['url']),
                    'ip' => $urlOk["ip"]
                ];

                $content = json_encode($content);

                return $this->send($content, 201);  
            }    
        }     
    }
    
    public function list(){      
        $header = apache_request_headers();
        $bugs = [];
        $bugManager = new BugManager($bugs);  

        if (isset($_GET['filtre']) && $_GET['filtre'] == 0) {
            $bugs = $bugManager->FindByStatus(0);
        } else {
            $bugs = $bugManager->findAll();
        }            

        $content = $this->CreateJson($bugs); 
        
        $content = json_encode($content);

        return $this->send($content, 201); 
    }
    
    public function show($id){  
        $bugs = [];
        $bugManager = new BugManager($bugs);
        $bug = $bugManager->find($id);         
     
        $content = $this->CreateJson($bug);         
        $content = json_encode($content);

        return $this->send($content, 201);
    }

    public function updateBug($id){       
        if(isset($_POST["typeUpdate"])){
            $bugs = [];
            $bugManager = new BugManager($bugs);
            $bug = $bugManager->find($id);            
            switch ($_POST["typeUpdate"]) {                
                case "no_resolve":
                    if(isset($_POST["status"])){
                        $bug->setStatus($_POST["status"]);
                    }  
                    $bugManager->updateBug($bug); 
                    http_response_code(200);
                    header("Content-type: application/json");
                    $response = [
                        'success' => true,
                        'id' => $bug->getId(),
                    ];
                    echo json_encode($response);
                    break;
                default:
                    http_response_code(404);
                    break;
            }
        }  
    }

    public function edit($id){
        $bugs = [];
        $bugManager = new BugManager($bugs);
        $bug = $bugManager->find($id);   
        parse_str(file_get_contents('php://input'), $_PATCH);
              
        if(!empty($_PATCH['titre']) || !empty($_PATCH['description'])){
            if(isset($_PATCH["titre"])){
                $bug->setTitre(htmlspecialchars($_PATCH["titre"]));
            }  
            if(isset($_PATCH["description"])){
                $bug->setDescription(htmlspecialchars($_PATCH["description"]));
            } 
            $bugManager->updateBug($bug); 

            $content = [
                'update' => true                
            ];

            $content = json_encode($content);

            return $this->send($content, 201); 
        }  
    }

    /**
     * Contrôle si le nom de domaine existe
     */
    public function recupIpByNomDomaine($nomDomaine)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://ip-api.com/json/'.$nomDomaine);
        
        $body = json_decode($response->getBody());

        if ($body->status == "success") {
            $rep = [
                'status' => $body->status,
                'ip' => $body->query,
            ];
        } else {
            $rep = [
                'status' => $body->status
            ];
        }
       
        return $rep;
    }

    /**
     * Contrôle la conformité de l'url à l'aide d'un regex
     */
    public function ControleUrl($url)
    {        
        if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?\/?/i', $url)) {
            $paramUrl = parse_url($url);                       
            $urlOk = $this->recupIpByNomDomaine($paramUrl["host"]);               
        } else {
            $urlOk = [
                'status' => "fail"
            ];
        }

        return $urlOk;
    }

    public function CreateJson($bugs)
    {
        $listBugs = [];      

        if (is_array($bugs ) && count($bugs) > 1) {
            foreach ($bugs as $bug) {  
                array_push(
                    $listBugs,
                    [
                        'id' => $bug->getId(),
                        'description' => $bug->getDescription(),
                        'titre' => $bug->getTitre(),
                        'createAt' => $bug->getCreateAt(),
                        'status' => $bug->getStatus()
                    ]
                );                  
            }

            

        }  else {
            $listBugs = 
            [
                'id' => $bugs->getId(),
                'description' => $bugs->getDescription(),
                'titre' => $bugs->getTitre(),
                'createAt' => $bugs->getCreateAt(),
                'status' => $bugs->getStatus()
            ];
        }   

        $content = [
            'success' => true,
            'bugs' => $listBugs,
        ];

        return $content;
    }
}

